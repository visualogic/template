﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TemplateProject.Models;
using TemplateProject.Models.Repos;

namespace TemplateProject.Infrastructure
{
    public class UniqueAttribute : ValidationAttribute
    {
        MembershipRepo dbRepo = new MembershipRepo();

        //dbContext db = new dbContext();
        private System.Reflection.PropertyInfo basePropertyInfo;

        public UniqueAttribute(string columnName)
        {
            _columnName = columnName;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            Boolean isExist = false;
            if (value == null)
            {
                return ValidationResult.Success;
            }
            else
            {
                _key = value.ToString();
            }
            int result = 0;
            
            
            if (_columnName == "UserName")
            {
                var data = dbRepo.GetUser(_key);
                if (data != null)
                {
                    result = data.UserID; //data.Where(u => u.UserName == _key).Select(u => u.UserID).FirstOrDefault();
                }
            }
            else if (_columnName == "Email")
            {
                var data = dbRepo.GetUserByEmail(_key);
                if (data != null)
                {
                    result = data.UserID; //db.Users.Where(u => u.Email == _key).Select(u => u.UserID).FirstOrDefault();
                }
            }           
            try
            {
                if (_columnName == "UserName" || _columnName == "Email")
                {
                    basePropertyInfo = validationContext.ObjectType.GetProperty("UserID");
                }
                
                var id = (int)basePropertyInfo.GetValue(validationContext.ObjectInstance, null);
                if (result != id && result != 0 || (result != 0 && id == 0))
                {
                    isExist = true;
                }                
            }
            catch (Exception err)
            {
                if (result != 0)
                {
                    isExist = true;
                }
            }
            if (isExist)
            {
                var errorMessage = FormatErrorMessage(value.ToString());
                return new ValidationResult(errorMessage);
            }
            else
            {
                return ValidationResult.Success;
            }
        }
        private string _key = "";
        private readonly string _columnName;
    }
}
