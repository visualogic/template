﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Xml;
using System.Xml.Serialization;
using TemplateProject.Models;

namespace TemplateProject.Helpers
{
    public static class Tools
    {
        private static dbContext db = new dbContext();
        public static string GetExt(string filename)
        {
            return filename.Substring(filename.LastIndexOf("."), filename.Length - filename.LastIndexOf("."));
        }

        public static string GetMd5Hash(MD5 md5Hash, string input)
        {

            // Convert the input string to a byte array and compute the hash. 
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));
            // Create a new Stringbuilder to collect the bytes 
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();
            // Loop through each byte of the hashed data  
            // and format each one as a hexadecimal string. 
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string. 
            return sBuilder.ToString();
        }

        public static string GetRandomNumber(int length)
        {
            string result = "";
            Random rnd = new Random();
            for (int i = 0; i < length; i++)
            {
                result += rnd.Next(0, 9).ToString();
            }
            return result;
        }

    }
}