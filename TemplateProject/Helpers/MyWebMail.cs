﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;

namespace TemplateProject.Helpers
{
    public class MyWebMail
    {
        public static string emailTo { get; set; }
        public static string emailCc { get; set; }
        public static string subject { get; set; }
        public static string body { get; set; }

        public static void sendMail()
        {
            WebMail.SmtpServer = "mail.tribasuki.net";
            WebMail.EnableSsl = false;
            WebMail.SmtpPort = 25;
            WebMail.UserName = "me@tribasuki.net";
            WebMail.Password = "tribasuki@74";
            WebMail.From = "me@tribasuki.net";
            WebMail.Send(
                to: emailTo,
                cc: emailCc,
                subject: subject,
                body: body,
                from: "me@tribasuki.net"
            );            
        }
    }
}