namespace TemplateProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial1 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Users", "InstituteCode");
            DropColumn("dbo.Users", "Subject1");
            DropColumn("dbo.Users", "Subject2");
            DropColumn("dbo.Users", "Subject3");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Users", "Subject3", c => c.Int(nullable: false));
            AddColumn("dbo.Users", "Subject2", c => c.Int(nullable: false));
            AddColumn("dbo.Users", "Subject1", c => c.Int(nullable: false));
            AddColumn("dbo.Users", "InstituteCode", c => c.String());
        }
    }
}
