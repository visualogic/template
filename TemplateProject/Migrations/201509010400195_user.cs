namespace QuestionBank.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class user : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Constructs",
                c => new
                    {
                        ConstructID = c.Int(nullable: false, identity: true),
                        SubjectCode = c.String(),
                        ConstructCode = c.String(),
                        ConstructDescription = c.String(),
                        DifficultyLevel = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ConstructID);
            
            CreateTable(
                "dbo.Contexts",
                c => new
                    {
                        ContextID = c.Int(nullable: false, identity: true),
                        SubjectCode = c.String(),
                        ConstructCode = c.String(),
                        ContextCode = c.String(),
                        ContextDescription = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        PersonInCharge = c.String(),
                    })
                .PrimaryKey(t => t.ContextID);
            
            CreateTable(
                "dbo.Institutes",
                c => new
                    {
                        InstituteID = c.Int(nullable: false, identity: true),
                        InstituteCode = c.String(),
                        InstituteName = c.String(),
                        AddLine1 = c.String(),
                        AddLine2 = c.String(),
                        AddLine3 = c.String(),
                        AddLine4 = c.String(),
                        Postcode = c.String(),
                        State = c.String(),
                    })
                .PrimaryKey(t => t.InstituteID);
            
            CreateTable(
                "dbo.Instructions",
                c => new
                    {
                        InstructionID = c.Int(nullable: false, identity: true),
                        BatchCode = c.String(),
                        ICNoChairman = c.String(),
                        ICNoLecturer = c.String(),
                        Semester = c.String(),
                        SubjectCode = c.String(),
                        CategoryOfQuestion = c.String(),
                        ConstructCode = c.String(),
                        ContextCode = c.String(),
                        TotalQuestion = c.String(),
                        TotalLecturerQuestion = c.String(),
                        TotalQuestionSubmitted = c.String(),
                        Dateline = c.DateTime(nullable: false),
                        InstructionDate = c.DateTime(nullable: false),
                        FlagInstruction = c.String(),
                    })
                .PrimaryKey(t => t.InstructionID);
            
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        RoleID = c.Int(nullable: false, identity: true),
                        RoleName = c.String(),
                    })
                .PrimaryKey(t => t.RoleID);
            
            CreateTable(
                "dbo.Subjects",
                c => new
                    {
                        SubjectID = c.Int(nullable: false, identity: true),
                        SubjectCode = c.String(),
                        SubjectName = c.String(),
                        CreditHour = c.String(),
                        Year = c.String(),
                        Semester = c.String(),
                    })
                .PrimaryKey(t => t.SubjectID);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UserID = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                        Password = c.String(),
                        FullName = c.String(),
                        ICNo = c.String(),
                        Email = c.String(),
                        AltEmail = c.String(),
                        ContactNo = c.String(),
                        RoleID = c.Int(nullable: false),
                        InstituteCode = c.String(),
                        Subject1 = c.String(),
                        Subject2 = c.String(),
                        Subject3 = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        ActivationCode = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.UserID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Users");
            DropTable("dbo.Subjects");
            DropTable("dbo.Roles");
            DropTable("dbo.Instructions");
            DropTable("dbo.Institutes");
            DropTable("dbo.Contexts");
            DropTable("dbo.Constructs");
        }
    }
}
