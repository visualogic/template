namespace QuestionBank.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class pending : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.States",
                c => new
                    {
                        StateID = c.Int(nullable: false, identity: true),
                        StateCode = c.String(),
                        StateName = c.String(),
                    })
                .PrimaryKey(t => t.StateID);
            
            AddColumn("dbo.Institutes", "StateCode", c => c.String());
            DropColumn("dbo.Institutes", "State");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Institutes", "State", c => c.String());
            DropColumn("dbo.Institutes", "StateCode");
            DropTable("dbo.States");
        }
    }
}
