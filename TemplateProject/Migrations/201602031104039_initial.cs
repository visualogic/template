namespace TemplateProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        RoleID = c.Int(nullable: false, identity: true),
                        RoleName = c.String(),
                    })
                .PrimaryKey(t => t.RoleID);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UserID = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                        Password = c.String(),
                        FullName = c.String(),
                        ICNo = c.String(),
                        Email = c.String(),
                        AltEmail = c.String(),
                        ContactNo = c.String(),
                        RoleID = c.Int(nullable: false),
                        InstituteCode = c.String(),
                        Subject1 = c.Int(nullable: false),
                        Subject2 = c.Int(nullable: false),
                        Subject3 = c.Int(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        ActivationCode = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.UserID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Users");
            DropTable("dbo.Roles");
        }
    }
}
