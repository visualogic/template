namespace TemplateProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class filter : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Filters",
                c => new
                    {
                        FilterID = c.Int(nullable: false, identity: true),
                        FilterName = c.String(),
                        FilterDesc = c.String(),
                    })
                .PrimaryKey(t => t.FilterID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Filters");
        }
    }
}
