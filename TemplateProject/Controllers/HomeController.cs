﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TemplateProject.Models;
using TemplateProject.Models.Repos;

namespace TemplateProject.Controllers
{
    public class HomeController : Controller
    {
        //dbContext db = new dbContext();
        MembershipRepo dbRepo = new MembershipRepo();

        public ActionResult Index(string info = "", string error = "")
        {
            if (Request.IsAuthenticated)
            {
                try
                {
                    String userName = Users.GetCurrentUserName();
                    User user = dbRepo.GetUser(userName); //db.Users.Where(u => u.UserName == userName).SingleOrDefault();
                    Role role = dbRepo.GetRole(user.RoleID); //db.Roles.Find(user.RoleID);
                    if (role.RoleName == "Administrator")
                    {
                        return RedirectToAction("Index", "Admin", new { info = info, error = error });
                    }
                    else if (role.RoleName == "Staff")
                    {
                        return RedirectToAction("Index", "Staff", new { info = info, error = error });
                    }
                    else if (role.RoleName == "User")
                    {
                        return RedirectToAction("Index", "Guest", new { info = info, error = error });
                    } 
                }
                catch (Exception err)
                {
                    ViewBag.error = err.Message;
                }
            }
            return RedirectToAction("Login", "Account");
        }

        public ActionResult Template()
        {
            return View();
        }
    }
}
