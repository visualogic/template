﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TemplateProject.Models;

namespace TemplateProject.Controllers
{
    public class PensyarahController : Controller
    {
        dbContext db = new dbContext();
        public ActionResult Index(string info = "", string error = "")
        {
            ViewBag.info = info;
            ViewBag.error = error;
            ViewBag.role = "User";
            return View();
        }

    }
}
