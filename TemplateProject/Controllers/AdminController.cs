﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TemplateProject.Models;
using System.Data.Entity;
using TemplateProject.Models.Repos;

namespace TemplateProject.Controllers
{
    public class AdminController : Controller
    {
        //dbContext db = new dbContext();
        MembershipRepo repo = new MembershipRepo();
        AdminRepo adminRepo = new AdminRepo();

        public ActionResult Index(string info = "", string error = "")
        {
            ViewBag.info = info;
            ViewBag.error = error;
            ViewBag.role = "Administrator";
            return View();
        }

        // ----------------------------------------------
        // --- Modified : 2015 07 27
        // --- By : Tri Basuki
        // ----------------------------------------------
        [HttpPost]
        public JsonResult GetAllUsers(string Filter1 = "", string SearchKey1 = "", string Filter2 = "", string SearchKey2 = "", 
            int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<UserModel> model = new List<UserModel>();
                List<User> data = repo.GetAllUsers().ToList();
                if (Filter1 != "")
                {
                    if (Filter1 == "UserName")
                    {
                        data = data.Where(u => u.UserName.ToLower().IndexOf(SearchKey1.Trim().ToLower()) >= 0).ToList();
                    }
                    if (Filter1 == "FullName")
                    {
                        data = data.Where(u => u.FullName.ToLower().IndexOf(SearchKey1.Trim().ToLower()) >= 0).ToList();
                    }
                    if (Filter1 == "RoleName")
                    {
                        var role = repo.GetAllUserRoles();
                        data = data.Where(u => role.Where(r => r.RoleID == u.RoleID)
                            .FirstOrDefault().RoleName.ToLower().IndexOf(SearchKey1.Trim().ToLower()) >= 0).ToList();
                    }
                                  
                }
                if (Filter2 != "")
                {
                    if (Filter2 == "UserName")
                    {
                        data = data.Where(u => u.UserName.ToLower().IndexOf(SearchKey2.Trim().ToLower()) >= 0).ToList();
                    }
                    if (Filter2 == "FullName")
                    {
                        data = data.Where(u => u.FullName.ToLower().IndexOf(SearchKey2.Trim().ToLower()) >= 0).ToList();
                    }
                    if (Filter2 == "RoleName")
                    {
                        var role = repo.GetAllUserRoles();
                        data = data.Where(u => role.Where(r => r.RoleID == u.RoleID)
                            .FirstOrDefault().RoleName.ToLower().IndexOf(SearchKey2.Trim().ToLower()) >= 0).ToList();
                    }    
                }
                
                foreach (User item in data)
                {
                    UserModel user = new UserModel
                    {
                        UserID = item.UserID,
                        UserName = item.UserName,
                        FullName = item.FullName,
                        RoleName = repo.GetRole(item.RoleID).RoleName,                       
                        IsActive = (item.IsActive == true ? "Aktif" : "Tidak Aktif"),
                    };
                    model.Add(user);
                }
                var modelCount = model.Count();
                switch (jtSorting)
                {
                    // Ascending
                    case "FullName ASC": model = model.OrderBy(c => c.FullName).
                        Skip(jtStartIndex).Take(jtPageSize).ToList(); break;
                    // Descending
                    case "FullName DESC": model = model.OrderByDescending(c => c.FullName).
                        Skip(jtStartIndex).Take(jtPageSize).ToList(); break;
                    // Ascending
                    case "RoleName ASC": model = model.OrderBy(c => c.RoleName).
                        Skip(jtStartIndex).Take(jtPageSize).ToList(); break;
                    // Descending
                    case "RoleName DESC": model = model.OrderByDescending(c => c.RoleName).
                        Skip(jtStartIndex).Take(jtPageSize).ToList(); break;
                }
                return Json(new { Result = "OK", Records = model, TotalRecordCount = modelCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        public JsonResult GetUserByID(int UserID)
        {
            try
            {
                var model = repo.GetUser(UserID);
                var RoleName = repo.GetRole(model.RoleID).RoleName;
                return Json(new { Result = "OK", record = model, role = RoleName });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        public JsonResult Activate(int UserID)
        {
            try
            {
                var model = repo.GetUser(UserID);
                adminRepo.Activate(model);
                return Json(new { Result = "OK", record = model });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        public JsonResult InActivate(int UserID)
        {
            try
            {
                var model = repo.GetUser(UserID);
                adminRepo.InActivate(model);
                return Json(new { Result = "OK", record = model });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        public JsonResult Remove(int UserID)
        {
            try
            {
                var model = repo.GetUser(UserID);
                adminRepo.Delete(model);
                return Json(new { Result = "OK", record = model });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
    }
}
