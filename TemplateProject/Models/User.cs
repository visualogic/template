﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TemplateProject.Models
{
    public class User
    {
        [Key]
        public int UserID { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string Password { get; set; }
        public string ICNo { get; set; }
        public string Email { get; set; }
        public string AltEmail { get; set; }
        public string ContactNo { get; set; }
        public int RoleID { get; set; }
        public bool IsActive { get; set; }
        public string ActivationCode { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}