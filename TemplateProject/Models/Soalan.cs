﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace QuestionBank.Models
{
    public class Soalan
    {
        [Key]
        public int QuestionID {get;set;}
        public int RoleID { get; set; } /// Pensyarah, Pengerusi, Panel and Admin Role
        public int InstructionID {get; set;}
        public int SubjectID { get; set; } // 
        public String CategoryOfQuestion {get;set;} // 1. Struktur, 2. Esei, 3. Objective
        public int UserID { get; set; }       // 3a Objective Pilihan Jawaban
        public int ConstructID { get; set; }   // 3b Objective Dwi Pilihan
        public int ContextID { get; set; }     // 3c1 Objective Berkait Type 1
        
        // Returned 
        public int ReturnCount { get; set; }
        public DateTime ReturnDate { get; set; }

        // Start Question fields
        public String QuestionText { get; set; }          // 3c2 Objective Berkait Type 2
        public String FinalAnswer { get; set; }

        public String QuestionStatus { get; set; }
        public DateTime QuestionProcessedDate { get; set; }        
        public DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; } // related to userID in user table

        // Struktur and Esai only use this all above fields

        // Objecive Pilihan Jawaban
        public String ObjectiveA { get; set; }
        public String ObjectiveB { get; set; }
        public String ObjectiveC { get; set; }
        public String ObjectiveD { get; set; }

        // Objective Dwi Pilihan
        public String Objective1 { get; set; }
        public String Objective2 { get; set; }
        public String Objective3 { get; set; }
        public String Objective4 { get; set; }

        // Objective Berkait Pilihan
        public int ObjectiveType { get; set; }
        public int QuestionInstructionID { get; set; }
        
        // Type 1
        public String QuestionSituationText { get; set; }
        

    }

    public class QuestionInstruction
    {
        [Key]
        public int QuestionInstructionID { get; set; }
        public String InstructionText { get; set; }
        // For Type 2 Only
        public String QuestionSituationText { get; set; }
    }
}