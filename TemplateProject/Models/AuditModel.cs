﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TemplateProject.Models
{
    public class AuditLog
    {
        [Key]
        [Required]
        public Guid AuditLogID { get; set; }
        public Guid SessionKey { get; set; }
        public string UserName { get; set; }
        public string IPAddress { get; set; }
        public string AreaAccessed { get; set; }
        public DateTime TimeAccessed { get; set; }
    }

    public class AuditDB
    {
        [Key]
        [Required]
        public Guid AuditDBID { get; set; }
        public int UserID { get; set; }
        public Guid SessionKey { get; set; }
        public DateTime EventDateUTC { get; set; }
        public string EventType { get; set; }
        public string TableName { get; set; }
        public string RecordID { get; set; }
        public string ColumnName { get; set; }
        public string OriginalValue { get; set; }
        public string NewValue { get; set; }
    }
}