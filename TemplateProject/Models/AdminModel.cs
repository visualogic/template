﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TemplateProject.Models
{
    public class UserModel
    {
        public int UserID { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string RoleName { get; set; }
        public string IsActive { get; set; }
    }
}