﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TemplateProject.Models
{
    public class RoleEnum
    {
        public static int Administrator = 1;
        public static int Staff = 2;
        public static int User = 3;
    }
}