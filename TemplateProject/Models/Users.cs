﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace TemplateProject.Models
{
    public class Users
    {
        private static dbContext db = new dbContext();

        public static String GetCurrentUserName()
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                return HttpContext.Current.User.Identity.Name;
            }
            return "";
        }

        public static string GetCurrentRoleName()
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                var username = HttpContext.Current.User.Identity.Name;
                using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["myData"].ConnectionString))
                {
                    var user = db.Query<User>("Select * From Users WHERE UserName = @UserName", new { username }).SingleOrDefault();
                    var role = db.Query<User>("Select * From Roles WHERE RoleID = @RoleID", new { user.RoleID }).SingleOrDefault();
                    return role.UserName;
                }
            }
            return "";
        }

        public static int GetCurrentRoleID()
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                var username = HttpContext.Current.User.Identity.Name;
                using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["myData"].ConnectionString))
                {
                    var user = db.Query<User>("Select * From Users WHERE UserName = @UserName", new { username }).SingleOrDefault();
                    return user.RoleID;
                }
            }
            return 0;
        }

        public static int GetCurrentUserID()
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                var username = HttpContext.Current.User.Identity.Name;
                using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["myData"].ConnectionString))
                {
                    var user = db.Query<User>("Select * From Users WHERE UserName = @UserName", new { username }).SingleOrDefault();
                    return user.UserID;
                }
            }
            return 0;
        }

        public static User GetCurrentUser()
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                var username = HttpContext.Current.User.Identity.Name;
                using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["myData"].ConnectionString))
                {
                    return db.Query<User>("Select * From Users WHERE UserName = @UserName", new { username }).SingleOrDefault();
                }
            }
            return null;
        }
        public static bool isActive(string username)
        {
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["myData"].ConnectionString))
            {
                var user = db.Query<User>("Select * From Users WHERE UserName = @UserName AND IsActive = 'true'", new { username }).ToList();
                return user.Count() > 0;
            }
        }
    }
}