﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace TemplateProject.Models
{
    public class dbContext : DbContext
    {
        public dbContext() : base("myData") { }
        public DbSet<Role> Roles { get; set; }
        public DbSet<User> Users { get; set; }
    }
}