﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using TemplateProject.Helpers;

namespace TemplateProject.Models.Repos
{
    public class MembershipRepo
    {

        private const string MissingRole = "Role does not exist";
        private const string MissingUser = "User does not exist";
        private const string TooManyUser = "User already exists";
        private const string TooManyRole = "Role already exists";
        private const string AssignedRole = "Cannot delete a role with assigned users";

        #region Properties
        public int NumberOfUsers
        {
            get
            {
                using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["myData"].ConnectionString))
                {                  
                    var data = db.Query<User>
                    ("Select Count(*) From Users").ToList();
                    return data.Count();
                }
                
            }
        }

        public int NumberOfRoles
        {
            get
            {
                using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["myData"].ConnectionString))
                {
                    var data = db.Query<User>
                    ("Select Count(*) From Roles").ToList();
                    return data.Count();
                }
            }
        }
        #endregion

        #region Query Methods
        public List<User> GetAllUsers()
        {
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["myData"].ConnectionString))
            {
                return db.Query<User>
                ("Select * From Users").ToList();
            }
        }

        public User GetUser(int id)
        {
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["myData"].ConnectionString))
            {
                return db.Query<User>("Select * From Users WHERE UserID = @Id", new { id }).SingleOrDefault();
            }
        }

        public User GetUser(string userName)
        {
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["myData"].ConnectionString))
            {
                return db.Query<User>("Select * From Users WHERE UserName = @UserName", new { userName }).SingleOrDefault();
            }
        }

        public User GetUser(string userName, string Email)
        {
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["myData"].ConnectionString))
            {
                return db.Query<User>("Select * From Users WHERE UserName = @UserName OR Email = @Email" , new { userName, Email }).SingleOrDefault();
            }
        }

        public User GetUserByEmail(string Email)
        {
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["myData"].ConnectionString))
            {
                return db.Query<User>("Select * From Users WHERE Email = @Email", new { Email }).SingleOrDefault();
            }
        }

        public List<User> GetUsersForRole(string roleName)
        {
            return GetUsersForRole(GetRole(roleName));

        }

        public List<User> GetUsersForRole(int id)
        {
            return GetUsersForRole(GetRole(id));
        }

        public List<User> GetUsersForRole(Role role)
        {
            if (!RoleExists(role))
                throw new ArgumentException(MissingRole);
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["myData"].ConnectionString))
            {
                return db.Query<User>("Select * From Users WHERE RoleID = @RoleID", new { role.RoleID }).ToList();
            }
        }

        public List<Role> GetAllUserRoles()
        {
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["myData"].ConnectionString))
            {
                return db.Query<Role>("Select * From Roles").ToList();
            }
        }

        public Role GetRole(int id)
        {
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["myData"].ConnectionString))
            {
                return db.Query<Role>("Select * From Roles WHERE RoleID = @Id", new { id }).SingleOrDefault();
            }
        }

        public Role GetRole(string name)
        {
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["myData"].ConnectionString))
            {
                return db.Query<Role>("Select * From Roles WHERE RoleName = @name", new { name }).SingleOrDefault();
            }
        }

        public Role GetRoleForUser(string userName)
        {
            return GetRoleForUser(GetUser(userName));
        }

        public Role GetRoleForUser(int id)
        {
            return GetRoleForUser(GetUser(id));
        }

        public Role GetRoleForUser(User user)
        {
            if (!UserExists(user))
                throw new ArgumentException(MissingUser);
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["myData"].ConnectionString))
            {
                return db.Query<Role>("Select * Role Users WHERE RoleID = @RoleID", new { user.RoleID }).SingleOrDefault();
            }
        }
        #endregion

        #region Insert/Delete

        private void AddUser(User user)
        {
            if (UserExists(user))
                throw new ArgumentException(TooManyUser);
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["myData"].ConnectionString))
            {
                string sqlQuery = "INSERT INTO Users (UserName, FullName, Password, Email, ContactNo, RoleID, IsActive, ActivationCode, " +
                    "CreatedData, ICNo, AltEmail) VALUES " +
                    "(@UserName, @FullName, @Password, @Email, @ContactNo, @RoleID, @IsActive, @ActivationCode, @CreateDate, )";
                db.Execute(sqlQuery, user);
            }
        }

        public void CreateUser(string username, string name, string password, string email, string contactNo, string roleName, string icNo, string altEmail, string instituteCode, int subject1, int subject2, int subject3)
        {
            Role role = GetRole(roleName);

            if (string.IsNullOrEmpty(username.Trim()))
                throw new ArgumentException("The user name provided is invalid. Please check the value and try again.");
            if (string.IsNullOrEmpty(name.Trim()))
                throw new ArgumentException("The name provided is invalid. Please check the value and try again.");
            if (string.IsNullOrEmpty(password.Trim()))
                throw new ArgumentException("The password provided is invalid. Please enter a valid password value.");
            if (string.IsNullOrEmpty(email.Trim()))
                throw new ArgumentException("The e-mail address provided is invalid. Please check the value and try again.");
            if (!RoleExists(role))
                throw new ArgumentException("The role selected for this user does not exist! Contact an administrator!");
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["myData"].ConnectionString))
            {
                var data = db.Query<User>("Select * From Users WHERE UserName = @UserName", new { username }).SingleOrDefault();
                if (data != null)
                    throw new ArgumentException("Username already exists. Please enter a different user name.");
            }
            
            if (string.IsNullOrEmpty(icNo.Trim()))
                throw new ArgumentException("The IC Number is invalid. Please check the value and try again.");

            using (MD5 md5Hash = MD5.Create())
            {
                User newUser = new User()
                {
                    UserName = username,
                    FullName = name,
                    Password = Tools.GetMd5Hash(md5Hash, password.Trim()),
                    Email = email,
                    ContactNo = contactNo,
                    RoleID = role.RoleID,
                    IsActive = false,
                    ActivationCode = Guid.NewGuid().ToString(),
                    CreatedDate = DateTime.Now,
                    ICNo = icNo,
                    AltEmail = altEmail
                };

                try
                {
                    using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["myData"].ConnectionString))
                    {
                        string sqlQuery = "INSERT INTO Users (UserName, FullName, Password, Email, ContactNo, RoleID, IsActive, ActivationCode, " + 
                            "CreatedDate, ICNo, AltEmail) VALUES " +
                            "(@UserName, @FullName, @Password, @Email, @ContactNo, @RoleID, @IsActive, @ActivationCode, @CreatedDate, @ICNo, @AltEmail)";
                        db.Execute(sqlQuery, newUser);
                    }
                }
                catch (ArgumentException ae)
                {
                    throw ae;
                }
                catch (Exception e)
                {
                    throw new ArgumentException("The authentication provider returned an error. Please verify your entry and try again. " +
                        "If the problem persists, please contact your system administrator.");
                }
            }
        }

        public void DeleteUser(User user)
        {
            if (!UserExists(user))
                throw new ArgumentException(MissingUser);
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["myData"].ConnectionString))
            {
                string sqlQuery = "DELETE Users WHERE UserID = @UserID";
                db.Execute(sqlQuery, user);
            }
        }

        public void DeleteUser(string userName)
        {
            DeleteUser(GetUser(userName));
        }

        public int GetUserID(string userName)
        {
            return GetUser(userName).UserID;
        }

        public void AddRole(Role role)
        {
            if (RoleExists(role))
                throw new ArgumentException(TooManyRole);
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["myData"].ConnectionString))
            {
                string sqlQuery = "INSERT INTO Roles (RoleName) VALUES (@RoleName)";
                int rowsAffected = db.Execute(sqlQuery, role);
            }
        }

        public void AddRole(string roleName)
        {
            Role role = new Role()
            {
                RoleName = roleName
            };

            AddRole(role);
        }

        public void DeleteRole(Role role)
        {
            if (!RoleExists(role))
                throw new ArgumentException(MissingRole);

            if (GetUsersForRole(role).Count() > 0)
                throw new ArgumentException(AssignedRole);
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["myData"].ConnectionString))
            {
                string sqlQuery = "DETELE Roles WHERE RoleID = @RoleID";
                db.Execute(sqlQuery, role);   
            }
        }

        public void DeleteRole(string roleName)
        {
            DeleteRole(GetRole(roleName));
        }

        #endregion

        #region Helper Methods

        public bool UserExists(User user)
        {
            if (user == null)
                return false;
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["myData"].ConnectionString))
            {
                var data = db.Query<User>("Select * From Users WHERE UserID = @Id OR UserName = @Name" , new { user.UserID, user.UserName }).SingleOrDefault();
                return data != null;
            }
        }

        public bool RoleExists(Role role)
        {
            if (role == null)
                return false;
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["myData"].ConnectionString))
            {
                var data = db.Query<Role>("Select * From Roles WHERE RoleID = @RoleID OR RoleName = @RoleName", new { role.RoleID, role.RoleName }).SingleOrDefault();
                return data != null;
            }
        }
        public bool UpdatePassword(User user)
        {
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["myData"].ConnectionString))
            {
                string sqlQuery = "UPDATE Users SET Password = @Password WHERE UserID = @UserID";
                int rowsAffected = db.Execute(sqlQuery, user);
                return rowsAffected > - 0;
            }
        }

        public bool UpdateUser(User user)
        {
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["myData"].ConnectionString))
            {
                string sqlQuery = "UPDATE Users SET RoleID = @RoleID, UserName = @UserName, FullName = @FullName, Email = @Email, " +
                    "ContactNo = @ContactNo, ICNo = @ICNo, AltEmail = @AltEmail WHERE UserID = @UserID";
                int rowsAffected = db.Execute(sqlQuery, user);
                return rowsAffected > -0;
            }
        }

        public User GetActivateUser(string activationCode)
        {
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["myData"].ConnectionString))
            {
                var model = db.Query<User>("Select * From Users WHERE ActivationCode = @ActivationCode AND IsActive = 'false'", new { activationCode }).SingleOrDefault();
                return model;
            }
        }
        #endregion
    }
}