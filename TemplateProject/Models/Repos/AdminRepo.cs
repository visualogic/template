﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace TemplateProject.Models.Repos
{
    public class AdminRepo
    {

        public int Activate(User user)
        {
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["myData"].ConnectionString))
            {
                string sqlQuery = "UPDATE Users SET IsActive = 'true' WHERE UserID = @UserID";
                int rowsAffected = db.Execute(sqlQuery, user);
                return rowsAffected;
            }
        }

        public int InActivate(User user)
        {
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["myData"].ConnectionString))
            {
                string sqlQuery = "UPDATE Users SET IsActive = 'false' WHERE UserID = @UserID";
                int rowsAffected = db.Execute(sqlQuery, user);
                return rowsAffected;
            }
        }

        public void Delete(User user)
        {
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["myData"].ConnectionString))
            {
                string sqlQuery = "DELETE Users WHERE UserID = @UserID";
                db.Execute(sqlQuery, user);
            }
        }

    }
}